docker_rebuild_app:
	cd docker/app && docker build -t volia_app .

docker_rebuild_mysql:
	cd docker/mysql && docker build -t volia_db .


docker_up:
	docker-compose up --build

