# php-pipeline

# Usage:

    make docker_rebuild_app
    make docker_rebuild_mysql
    make docker_up

Open http://172.22.12.11 in browser.


# Key file locations:

Frontend part: www/resources/js/

App backend controllers: www/app/Http/Controllers/

App domain logic: www/app/Domain/

App routes : www/routes
