<?php

namespace App\Domain;

use App\Models\CountryList;
use App\Models\Country;

class CountryListProvider {

    public function __construct($db) {
        $this->db = $db;
    }


    /**
     * Loads country list from database
     */
    public function createFromDatabase() {
        $countries = $this->db->select("SELECT * FROM countries");

        $countryArray = [];
        foreach ($countries as $c) {
            $countryArray[] = new Country($c->country, $c->capital);
        }

        $countryList = new CountryList($countryArray);
        return $countryList;
    }


    /**
     * Saves country list to database
     */
    public function saveToDatabase(CountryList $list) {
        $this->db->table('countries')->delete();

        $this->db->table('countries')->insert(
            $list->serialize()
        );
    }

}
