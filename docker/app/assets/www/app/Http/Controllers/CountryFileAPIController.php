<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domain\Converter\Converter;
use App\Domain\CountryListProvider;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class CountryFileAPIController extends BaseController
{
    /**
     * Parse file with country list and return JS friendly data structure
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $file = $request->file('fileToUpload');
        $converter = new Converter();

        try {
            $converted = $converter->load(
                $file->path(),
                strtolower($file->getClientOriginalExtension())
            );
        } catch (\Exception $e) {
            return response()->json([
                "error" => "Wrong file format"
            ]);
        }

        return response()->json(
            $converted->serialize()
        );
    }

    /**
     * Serialize JS data structure into one of the choosen file formats
     *
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $format = $data["format"];
        
        $countryList = json_encode($data["countryList"]);

        $tmpFileName = "tmp_file_" . microtime();
        Storage::disk('public')->put($tmpFileName, $countryList);

        $converter = new Converter();

        $fileData = "";
        
        try {
            $countryList = $converter->load(Storage::disk('public')->path($tmpFileName), 'json');
            $fileData = $converter->save($countryList, $format);
        } finally {
            Storage::disk('public')->delete($tmpFileName);
        }

        return response()->json(
            [
                "mime" => $converter->getMIMETypeForExtension($format),
                "data" => $fileData
            ]
        );
    }


    /**
     * Get list of available formats
     *
     * @return \Illuminate\Http\Response
     */
    public function listFormats()
    {
        return response()->json(
            (new Converter())->getAvailableFormats()
        );
    }


    /**
     * Get list of available formats
     *
     * @return \Illuminate\Http\Response
     */
    public function loadCountries()
    {
        $provider = new CountryListProvider(
            DB::connection()
        );

        $cl = $provider->createFromDatabase();

        return response()->json(
            $cl->serialize()
        );
        
    }


    /**
     * Get list of available formats
     *
     * @return \Illuminate\Http\Response
     */
    public function saveCountries(Request $request)
    {       
        $provider = new CountryListProvider(DB::connection());
        $countryList = null;

        $converter = new Converter();
        
        try {
            $tmpFileName = "tmp_file_" . microtime();
            Storage::disk('public')->put($tmpFileName, $request->getContent());

            $countryList = $converter->load(Storage::disk('public')->path($tmpFileName), 'json');
        } finally {
            Storage::disk('public')->delete($tmpFileName);
        }

        $provider->saveToDatabase($countryList);

        return response()->json([
            'status' => "ok"
        ]);
        
    }

}
